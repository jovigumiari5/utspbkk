<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProsesController extends Controller
{
    public function create(){
        return view('create021190072');
    }


    public function proses(Request $request){
            $data = array();
            $data ['npm'] = $request->npm;
            $data['nama'] = $request->nama;
            $data['programstudi'] = $request->programstudi;
            $data['NOHP'] = $request->NOHP;
            $data['TTL'] = $request->TTL;
            $data['jeniskelamin'] = $request->jeniskelamin;
            $data['Agama'] = $request->Agama;

            return view('view021190072', ['data' => $data]);
        }
}
